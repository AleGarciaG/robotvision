Practical exercise:

- We began a Sherlock inspection in class where images with 3 dots defined a path of a Robot. 

-Instructions:
--Write a Sherlock script that draws the line segments between the dots to form a triangles
--Obtain the distance of the vertices and write on the screen.
--Using the law of cosines, calculate the angles in degrees and write them next to each vertex of the screen.

Due date: 15-Oct-2020