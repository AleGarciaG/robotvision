//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//        This program reads an input grayscale image in *.pgm format
//        and converts it into a negative grayscale image in *.pgm format
//        by taking the one's complement of every pixel in the file.
//
//------------------------------------------------------------------------------
//                  Version updated: August 24, 2009
//                               RickWare
//------------------------------------------------------------------------------

//==============================================================================
//------------------------P R O G R A M-----------------------------------------
//==============================================================================
//
//  main();
//     heading();               display program heading
//     openfiles();             opens input & output image files
//     negative();              convert original image to negative
//     readhdr();               reads input image file header
//     closefiles();            closes input and processed output image files
//------------------------------------------------------------------------------
void     heading(void);
void     openfiles(void);
void     negative(void);
void     readhdr(void);
void     closefiles();
//------------------------------------------------------------------------------
# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>
#include <bits/stdc++.h> 
//------------------------------------------------------------------------------
int      MRows, NCols;                     //indices for rows and cols
FILE     *infptr, *outfptr;                //input/output file pointers
char     infile[40];
char     static  outfile[40];              //names of input/output image files

//------------------------------------------------------------------------------
//-----------------------------MAIN---------------------------------------------
//------------------------------------------------------------------------------
main()
{
    //--------------( Display Heading with information )------------------------
    heading();
    //---------------(Open Input & Output Image Files)--------------------------
    openfiles();
    //---------------------(Obtain negative of image)---------------------------
    negative();
    //------------------(Close Any Open Image Files)----------------------------
    closefiles();
    //--------------------------------------------------------------------------
    printf("\n Bye! Bye!\n");
    system("PAUSE");
    return (1);
} //end main()

//------------------------------------------------------------------------------
//----------------------------HEADING-------------------------------------------
//------------------------------------------------------------------------------
void heading()
{ int      i;
  for (i=0;i<16;i++) printf("                                     +\n");
  printf("                       Digital Image Processing Program\n");
  printf("                         Updated: August 24, 2009\n");
  printf("                                 RickWare\n");
  for (i=0;i<4;i++)  printf("                                     +\n");
}//end heading()

//------------------------------------------------------------------------------
//--------------------------OPENFILES-------------------------------------------
//------------------------------------------------------------------------------
void openfiles(void)
{
  printf("\n                 OPEN an image file\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf(" Enter name of *.pgm INPUT image file (example: lena.pgm) ? ");
  scanf("%s",&infile);
  printf(" Enter name of *.pgm OUTPUT image file (example: lenaconv.pgm ? ");
  scanf("%s",&outfile);
    
  if ((infptr = fopen(infile, "rb")) == NULL)
  { printf(" Can NOT open input image file: <%s>\n",infile);
    printf(" Exiting program..... "); system("PAUSE"); exit(1);
  }
  else  printf(" Input file <%s> opened sucessfully\n\n",infile);
  
  if ((outfptr = fopen(outfile,"wb")) == NULL)
  { printf(" Can NOT open output image file <%s>\n\n",outfile);
    printf(" Exiting program....."); system("PAUSE"); exit(1);
  }
  else printf(" Output file <%s> is opened sucessfully\n\n",outfile);
  
  readhdr();
  return;
}//end openfiles()

//------------------------------------------------------------------------------
//------------------------------READHDR-----------------------------------------
//------------------------------------------------------------------------------
void readhdr()
{
  int     i, k=0, MaxRGB;
  char    c, c1, buffer[128];
  //-----------------------[Read PPM File Header]-------------------------------
  printf("\n\n File <%s> Header Bytes:\n",infile);
  printf("------------------------------------------------------------\n");
    i = 0;
    do
    { c = fgetc(infptr);
      buffer[i] = c; i++;
    } while (c != '\n');
    
     c1 = buffer[1];
     
     if (c1 == '5')
     {
       printf("\nFile type is:  <P%c>\n",c1);
     }
     else
     { printf(" Image is in WRONG format!! Quitting.........\n\n");
       system("PAUSE");
       exit(0);
     }
    
  fscanf(infptr,"%d %d %d",&NCols, &MRows, &MaxRGB);
  c = fgetc(infptr);
  printf("%d ",NCols);
  printf("%d     <-----(Width & Height)\n", MRows);
  printf("%d         <-----(Max. RGB Level)\n\n",MaxRGB);
}//end readhdr()

//------------------------------------------------------------------------------
//--------------------------Obtain morphologyImage image------------------------------
//---------------by taking the one's complement of every pixel------------------
//------------------------------------------------------------------------------
void negative()
{
  unsigned char Gray;
  //unsigned char mat [512][512];
  //unsigned char morphologyImage[512][512];
  unsigned char* ArrayPixel= (unsigned char*) malloc((MRows*NCols)*sizeof(unsigned char));
  unsigned char* morphologyImage= (unsigned char*) malloc((MRows*NCols)*sizeof(unsigned char));

			 
  int col, row, value;

  //Add *.pgm Header to output file
  fprintf(outfptr,"P5\n%d %d\n255\n",NCols,MRows);
  
  //Read input image and store en matrix
  for (row = 0; row <MRows; row++) {
    for (col = 0; col < NCols ; col++){
        Gray = fgetc(infptr);
        int p = ((NCols*row) + col);
        *(ArrayPixel+p)= Gray;
    }
  }

  
  // Clear Morphology matrix
 for (row = 0; row <MRows; row++) {
    for (col = 0; col < NCols ; col++){
        int p = ((NCols*row) + col);
        *(morphologyImage+p)= 0;
    }
  }
  
  
  //Morphology Image Dilation
  /*
    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==255)||(*(ArrayPixel+(p-NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)
         ||(*(ArrayPixel+(p-1))==255)      ||(*(ArrayPixel+(p))==255)      ||(*(ArrayPixel+(p+1))==255)
         ||(*(ArrayPixel+(p+NCols-1))==255)||(*(ArrayPixel+(p+NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)){
            *(morphologyImage+p)=255;
        }else{
            *(morphologyImage+p)=0;
        }
      }
    }
    */
  /*
  //Morphology Image Erosion

    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==0)||(*(ArrayPixel+(p-NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)
         ||(*(ArrayPixel+(p-1))==0)      ||(*(ArrayPixel+(p))==0)      ||(*(ArrayPixel+(p+1))==0)
         ||(*(ArrayPixel+(p+NCols-1))==0)||(*(ArrayPixel+(p+NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)){
            *(morphologyImage+p)=0;
        }else{
            *(morphologyImage+p)=255;
        }
      }
    }
  */
 /*
  //Morphology Image opening
    //Morphology Image Erosion

    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==0)||(*(ArrayPixel+(p-NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)
         ||(*(ArrayPixel+(p-1))==0)      ||(*(ArrayPixel+(p))==0)      ||(*(ArrayPixel+(p+1))==0)
         ||(*(ArrayPixel+(p+NCols-1))==0)||(*(ArrayPixel+(p+NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)){
            *(morphologyImage+p)=0;
        }else{
            *(morphologyImage+p)=255;
        }
      }
    }
    //Save the erosion image in the original image
     for (row = 0; row <MRows; row++) {
    for (col = 0; col < NCols ; col++){
        int p = ((NCols*row) + col);
        *(ArrayPixel+p)=*(morphologyImage+p);
    }
  }

    //Morphology Image Dilation
    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==255)||(*(ArrayPixel+(p-NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)
         ||(*(ArrayPixel+(p-1))==255)      ||(*(ArrayPixel+(p))==255)      ||(*(ArrayPixel+(p+1))==255)
         ||(*(ArrayPixel+(p+NCols-1))==255)||(*(ArrayPixel+(p+NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)){
            *(morphologyImage+p)=255;
        }else{
            *(morphologyImage+p)=0;
        }
      }
    }
    */
  //Morphology Image close
    //Morphology Image Dilation
    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==255)||(*(ArrayPixel+(p-NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)
         ||(*(ArrayPixel+(p-1))==255)      ||(*(ArrayPixel+(p))==255)      ||(*(ArrayPixel+(p+1))==255)
         ||(*(ArrayPixel+(p+NCols-1))==255)||(*(ArrayPixel+(p+NCols))==255)||(*(ArrayPixel+(p+NCols+1))==255)){
            *(morphologyImage+p)=255;
        }else{
            *(morphologyImage+p)=0;
        }
      }
    }
    //Save the Dilation image in the original image
    for (row = 0; row <MRows; row++) {
      for (col = 0; col < NCols ; col++){
          int p = ((NCols*row) + col);
          *(ArrayPixel+p)=*(morphologyImage+p);
      }
    }
    //Morphology Image Erosion
    for (row = 1; row < MRows-1; row++){
      for (col = 1; col < NCols-1; col++) {
        int p = ((NCols*row) + col);
        if((*(ArrayPixel+(p-NCols-1))==0)||(*(ArrayPixel+(p-NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)
         ||(*(ArrayPixel+(p-1))==0)      ||(*(ArrayPixel+(p))==0)      ||(*(ArrayPixel+(p+1))==0)
         ||(*(ArrayPixel+(p+NCols-1))==0)||(*(ArrayPixel+(p+NCols))==0)||(*(ArrayPixel+(p+NCols+1))==0)){
            *(morphologyImage+p)=0;
        }else{
            *(morphologyImage+p)=255;
        }
      }
    }

  //Save Morphology image 
  for (row = 0; row < MRows; row++)
    for (col = 0; col < NCols; col++) {
        int p = ((NCols*row) + col);
       Gray= *(morphologyImage+p);
        fputc(Gray,outfptr);
    }   
}

//------------------------------------------------------------------------------
//----------------------------CLOSEFILES----------------------------------------
//------------------------------------------------------------------------------
void closefiles()
{ //------------------------(Close Files)---------------------------------------
  fclose(infptr);
  fclose(outfptr);

  return;
} //end closefiles()
//------------------------------------------------------------------------------
