
//----------------------------------------------------------------------------//
// Template for reading portable gray map files (*.pgm)                       //
//                                                                            //
//                                                RickWare                    //
//                                                August 24, 2020             //
//                                                                            //
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
//         Prototype declarations                                             //
//----------------------------------------------------------------------------//

void heading (void);      /* Displays program heading */
void openfiles (void);    /* Opens input and output files in binary */
void userdefined (void);  /* Here, the user will implement his routine */
void readhdr (void);      /* Read header from input image file */
void addhdr (void);       /* Add header to output image file */
void closefiles (void);   /* Close all files used in program */

//----------------------------------------------------------------------------//
//         Include declarations                                               //
//----------------------------------------------------------------------------//

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <string.h>

//----------------------------------------------------------------------------//
//         Variable declarations                                              //
//----------------------------------------------------------------------------//

int    MRows, NCols;                 /* Number of Rows and columns in image */
FILE   *infptr, *outfptr, *outfptrh; /* Input and output file pointers */
char   infile[40];                   /* Name of input file */
char   outfile[40];                  /* Name of output file */

//----------------------------------------------------------------------------//
//         Main program                                                       //
//----------------------------------------------------------------------------//

main ()
{
     // Display heading
     heading ();
     // Open Input and Output Image Files
     openfiles ();
     // Read header from image file
     readhdr();
     // Add header to output file
     addhdr();
     // Execute userdefined procedure
     userdefined ();
     // Close all image files
     closefiles ();
     
     printf("\n Bye! Bye!\n");
     system("PAUSE");
     return(1);
} // end main ();

//----------------------------------------------------------------------------//
//         Headings                                                           //
//----------------------------------------------------------------------------//

void heading ()
{
     int i;
     for (i=0; i<16; i++)
       printf("                                    +                       \n");
       printf("                      Digital Image Processing in C         \n");
       printf("                          Date: August 24, 2020             \n");
       printf("                            Author: RickWare                \n");
     for (i=0; i<3; i++)
       printf("                                    +                       \n");
       return;
} // end heading ()

//----------------------------------------------------------------------------//
//         Openfiles                                                          //
//----------------------------------------------------------------------------//

void openfiles ()
{
     printf("\n Opening Input and Output image files\n");
     printf(" Enter name of *.pgm INPUT image file (example: lena.pgm) ");
     scanf("%s",&infile);
     
     //Check if input file exists
     if ((infptr = fopen(infile, "rb")) == NULL)
     {
       printf(" Cannot open input image file: %s\n",infile);
       printf(" Exiting program...\n");
       system("PAUSE");
       exit(1);
       }
     
     printf(" Enter name of *.pgm OUTPUT image file (example: lenaout.pgm) ");
     scanf("%s",&outfile);
     
     //Check if output file was created succesfully
     if ((outfptr = fopen(outfile, "wb")) == NULL)
     {
       printf(" Cannot create output image file: %s\n",outfile);
       printf(" Exiting program...\n");
       system("PAUSE");
       exit(1);
       }
       
     // If this point is reached, file are OK
     printf(" File opened and created OK! \n\n");
     
}  //end openfiles ()

//----------------------------------------------------------------------------//
//         Read Header                                                        //
//----------------------------------------------------------------------------//

void readhdr ()
{
     int   i=0, MaxRGB;
     char  c, buffer[128];
     
     //Read header of PGM file
     printf("\n\n File <%s> Header: \n",infile);
     
     do
     {
       c = fgetc(infptr);
       buffer[i] = c;
       i++; 
     } while (c != '\n');
     
     //Check if file is P5 (pgm) format
     if (buffer[1] == '5')
       printf("\n Input file is pgm, OK\n");
     else
     {
       printf("\n Input file is NOT pgm, Exiting program...\n");
       system("PAUSE");
       exit(0);
     }
     
     //File is pgm, read rest of header
     fscanf(infptr,"%d %d %d",&NCols, &MRows, &MaxRGB);
     c = fgetc(infptr);
     printf("%d ",NCols);
     printf("%d   <---- Width x Height) \n",MRows);
     printf("%d   <---- Max. Grayscale level) \n\n",MaxRGB);
}  // end readhdr ()

//----------------------------------------------------------------------------//
//         Add Header                                                         //
//----------------------------------------------------------------------------//

void addhdr ()
{
    fprintf(outfptr, "P5\n%d %d\n%d\n",NCols,MRows,255);

} //addhdr ()

//----------------------------------------------------------------------------//
//        Function for histogram equailization of a grey scale image                                             //
//----------------------------------------------------------------------------//

void userdefined ()
{
  	//local variabkes and constants
      //Pixel from oroginal image
	  unsigned char Pixel;
      //size of the image
	  int s = MRows*NCols;
      //Variables to move around the matrix and it. the matrix has the data of the image input
    unsigned char* ArrayPixel= (unsigned char*) malloc((s)*sizeof(unsigned char));
        //row
    int r = 0;
        //colum
    int c = 0;
    //counter of the number of pixel por pixel value in grey scale
     int histEqualization[256];
	//Read pixel of the input image
	Pixel = fgetc(infptr);
    //set the pixel counter to 0
   	for (int i = 0; i<256; i++){
        histEqualization[i] = 0;
     }

	do{
    //count the pixels
		histEqualization[Pixel]++;   // Count the pixel 
    //save the image
      //get the posicion where to sabe t=he data
      int p = ((NCols*r) + c);
      //save the data
		*(ArrayPixel+p)=Pixel;
      //move to next colum of the matrix
    c++;
      //if it gets to last colum jump to next row and first colum
    if(c>(NCols-1)){
      c=0;
      r++;
    }
		Pixel = fgetc(infptr);//Read next input image Pixel and chek if its the last
	}while(!feof(infptr));

  //make tha acomulative frequency of the pixels counted
  for (int pi=1;pi<256;pi++){
      histEqualization[pi]=histEqualization[pi]+histEqualization[pi-1];
  }
  //make the normalized frecuency and define the new pixel value in one step
  for (int pi=0;pi<256;pi++){
      histEqualization[pi]=round((255.0*histEqualization[pi])/s);
  }
  //save the new values in the new image
    //move colums
    //move rows
  for (int ri=0;ri!=MRows;ri++){
      for (int ci=0;ci!=NCols;ci++){
      //get the pixel value of image matrix 
      int p = ((NCols*ri) + ci);
      int originalPixel = *(ArrayPixel+p);
      //get and print in the new image file the value of the equivalent of original pixel in its Histogram Equalization form
      //the equivalen of the pixel in Histogram Equalization is saved in the array histEqualization where the posicion 
      //represent the original pixel value (value in gray scale) and value in that posicion is the new value for the pixel
      fputc(histEqualization[originalPixel],outfptr);//Store the modified Pixel in output image

    }
  }
	

  free(ArrayPixel);

}  // end userdefined ()

//----------------------------------------------------------------------------//
//         Close Files                                                        //
//----------------------------------------------------------------------------//

void closefiles ()
{
     fclose(infptr);
     fclose(outfptr);
}  // end closefiles ()
