Practical exercise:

-The images were obtained using a 640 x 480 8-bit monochromatic camera

-Instructions:
--Count the number of parts
--Write a script to determine whether the number of parts gives a prime number
--If number of parts gives a prime number, send a logic "1" to Channel two in 
  order to reject the parts
--If number of parts is not a prime number, send a logic "0" to channel two to
  accept the parts

Due date: 5-Oct-2020