Anaconda includes: Jupyter Lab, Jupyter Notebook and Spyder (all based on Python)

1) Install Anaconda v3_5_0_0, make sure you install it at c:\Anaconda level
2) Anaconda comes with version 3.3 of Python, it has to be downgraded to version 2.7.8 to work with OpenCV
3) To downgrade execute: "Anaconda Prompt", a cmd window will open (see word document found in folder)
4) Type the following from the command window:
	conda install python=2.7.8  (it will take a while, be patient and prompt yes)
	conda install numpy	    (needed for fast operations in OpenCV)
	conda install -c conda-forge matplotlib = 2.0.2 (needed in some OpenCV programs
5) Decompress OpenCV v3.0.0 into c:\opencv by clicking on "opencv-3.0.0" (see video in directory)
6) Open the environment variables and do the following	(see same video as point 5) in directory
	Create a new system environment variables:
		Name: OPENCV_DIR
		Value: c:\opencv\build\x64\vc12
	Edit and place the cursos at the end of the "path" variable and add
		;%OPENCV_DIR%\bin

7) Copy and Paste the cv2.pyd file (See Video in directory)
The Anaconda Site-packages directory (e.g. C:\Anaconda\Lib\site-packages in my case) contains the Python packages that you may import. Our goal is to copy and paste the  cv2.pyd file to this directory (so that we can use the import cv2 in our Python codes.).
To do this, copy the cv2.pyd file...
From this OpenCV directory (the beginning part might be slightly different on your machine):

# Python 2.7 and 32-bit machine: 
C:\opencv\build\python\2.7\x84

# Python 2.7 and 64-bit machine: 
C:\opencv\build\python\2.7\x64

To this Anaconda directory (the beginning part might be slightly different on your machine):
C:\Anaconda\Lib\site-packages